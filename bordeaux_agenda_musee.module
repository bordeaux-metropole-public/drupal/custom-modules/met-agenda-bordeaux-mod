<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\bordeaux_agenda_musee\Storage\BordeauxAgendaEntityStorage;
use Drupal\bordeaux_agenda_musee\Storage\BordeauxAgendaStorage;

/**
 * Implements hook_menu().
 * @return array
 */
function bordeaux_agenda_musee_menu() {
  return array(
    'admin/config/services/bordeaux-agenda' => array(
      'title' => t('Bordeaux agenda settings'),
      'route_name' => 'admin.settings_form',
    ),
    'admin/config/services/bordeaux-agenda/' => array(
      'title' => t('Bordeaux agenda settings'),
      'route_name' => 'admin.settings_form',
    ),
    'admin/reports/bordeaux-agenda' => array(
      'title' => t('Bordeaux agenda Logs'),
      'route_name' => 'admin.logs',
    ),
  );
}

/**
 * Ajout du formulaire dans le node
 *
 * @return array
 */
function bordeaux_agenda_musee_form_alter(&$form, $form_state, $form_id) {
  if ($form_state->getFormObject() instanceof EntityFormInterface && $form_state->getformObject()->getEntity()) {
    if(get_available_form_config($form_state, 'bordeaux_agenda_musee.node')) {
      getFormAgenda($form, $form_state, 'node', $form_state->getformObject()->getEntity()->id());
    } elseif (get_available_form_config($form_state, 'bordeaux_agenda_musee.entity')) {
      getFormAgenda($form, $form_state, $form_state->getformObject()->getEntity()->getEntityType()->id(), $form_state->getformObject()->getEntity()->getEntityType()->id());
    }
  }
}

/**
 * @param $form
 * @param $form_state
 * @param $entityId
 * @param $bundleId
 */
function getFormAgenda(&$form, $form_state, $entityId, $bundleId) {

  /* Ajout du formulaire Bordeaux Agenda et de la partie token que pour un ajout / envoi. */
  $buildInfo = $form_state->getBuildInfo()['callback_object'];

  /* On vérifie si l'événement a déjà été envoyé à Bordeaux.FR */
  $nidStorage = BordeauxAgendaStorage::getPostId($form_state->getformObject()->getEntity()->id(), $form_state->getformObject()->getEntity()->getEntityType()->id());


  if($nidStorage) {
    $form['bam_info_context'] = [
      '#type' => 'item',
      '#title' => '<img class="bam-logoinform" src="/modules/custom/bordeauxagendamusee/img/logovillebordeauxmini.jpg"/> '.
        t('Evenement déjà envoyé à BORDEAUX.FR sous l identifiant '.$nidStorage.' '),
      '#description' => t("Cocher cette case et la modification de cet événement entrainera un mail à destination de l'eCom pour indiquer que ce contenu a été modifié"),
      '#weight' => 59,
    ];
    $form['bam_info_context']['notificationmail'] = [
      '#type' => 'checkbox',
      '#title' => t('Envoyer une notification mail à l\'eCom'),
      '#default_value' => FALSE,
    ];
  } else {
    /* on affiche le formulaire si l'événement n'a pas encore été transmis ou si c'est un ajout de contenu */
    $tokens = [];
    $agendaNodeType = Drupal::service('entity_field.manager')->getFieldDefinitions($entityId, $bundleId);
    foreach ($agendaNodeType as $key => $field_definition) {
      $tokens[$key]['label'] = is_string($field_definition->getLabel()) ? $field_definition->getLabel() : $key;
      $tokens[$key]['key'] = '[' . $entityId . ':' . $key . ']';
    }
    if ($entityId == 'node') {
      $form['advanced']['token_container'] = [
        '#type' => 'details',
        '#title' => t('Tokens du contenu'),
        '#open'=>FALSE,
        '#weight' => 150,
      ];
      $form['advanced']['token_container']['tokens'] = [
        '#type' => 'table',
        '#header' => ['label', 'key'],
        '#rows' => $tokens,
        '#empty' => 'Aucuns token liés à cette recherche',
        '#open'=>FALSE,
      ];
    } else {
      $form['token_container'] = [
        '#type' => 'details',
        '#title' => t('Tokens du contenu'),
        '#open'=>FALSE,
        '#weight' => 150,
      ];
      $form['token_container']['tokens'] = [
        '#type' => 'table',
        '#header' => ['label', 'key'],
        '#rows' => $tokens,
        '#empty' => 'Aucuns token liés à cette recherche',
        '#open'=>FALSE,
      ];
    }
    $agendaNodeForm = Drupal::service('bordeaux_agenda_musee.form');
    $form = $agendaNodeForm->getForm($form, $form_state);
  }
  $form['#attached']['library'][] = 'bordeaux_agenda_musee/form_node';
  $form['actions']['#weight'] = 70;
  $form['actions']['submit']['#submit'][] = $entityId == 'node' ?
    'bordeaux_agenda_musee_form_node_submit' : 'bordeaux_agenda_musee_form_entity_submit';
}

/**
 * Vérification d'un envoi disponible et activé
 * @param string $type
 * @param string $form_id
 * @param $config
 * @return bool
 */
function get_available_form_config($form_state, $config) {
  $buildInfo = $form_state->getBuildInfo()['callback_object'];

  // vérification du type de formulaire (default = add/new)
  if ($buildInfo->getOperation() != 'default' && $buildInfo->getOperation() != 'add' && $buildInfo->getOperation() != 'edit') {
    return false;
  }

  $configuration = Drupal::config($config);
  $id = $form_state->getformObject()->getEntity()->getEntityType()->id() == 'node' ?
     $form_state->getformObject()->getEntity()->getType() : $form_state->getformObject()->getEntity()->getEntityType()->id();

  // vérification si l'entité est présente en configuration
  if(!array_key_exists($id, $configuration->get())) {
    return false;
  }

  if($configuration->get()[$id]['configuration']['active']) {
    return true;
  }

  return false;
}

/**
 * Appel du service AgendaNodeSubmit pour l'encapsulation des données et l'envoi sur bordeaux.agenda
 *
 * @return array
 */
function bordeaux_agenda_musee_form_node_submit($form, FormStateInterface $form_state) {
  bordeaux_agenda_musee_form_submit($form_state, 'bordeaux_agenda_musee.node', $form_state->getformObject()->getEntity()->getType());
}

/**
 * Appel du service AgendaNodeSubmit pour l'encapsulation des données et l'envoi sur bordeaux.agenda
 *
 * @return array
 */
function bordeaux_agenda_musee_form_entity_submit($form, FormStateInterface $form_state) {
  $bundleId = $form_state->getformObject()->getEntity()->getEntityType()->id();
  bordeaux_agenda_musee_form_submit($form_state, 'bordeaux_agenda_musee.entity', $bundleId);
}

/**
 * @param FormStateInterface $form_state
 * @param $configName
 */
function bordeaux_agenda_musee_form_submit(FormStateInterface $form_state, $configName, $entityType) {
  $config = Drupal::config($configName);
  $configNode = $config->get($entityType);
  /* Si le noeud est configuré pour être envoyé à Bdx Agenda, on le traite (envoi événement ou mél). */
  if($configNode && $configNode['configuration']['active']) {
    /** @var \Drupal\bordeaux_agenda_musee\Service\AgendaSubmitService $agendaSubmitForm */
    $agendaSubmitForm = Drupal::service('bordeaux_agenda_musee.submit');
    $agendaSubmitForm->sendData($form_state);
  }
}

/**
 * Envoi du Mail
 *
 * @param $key
 * @param $message
 * @param $params
 */
function bordeaux_agenda_musee_mail($key, &$message, $params) {
  if ($key == 'bordeaux_agenda_musee.request.notice') {
    /** @var EntityInterface $entity */
    $entity = $params['entity'];
    $user = $params['user'];

    $url = Url::fromRoute(
      'entity.' . $params['entity_type'] . '.canonical', [$params['entity_type'] => $entity->id()], ['absolute' => TRUE]);
    $changed = $entity->changed->first()->getValue();

    $node_changed = date('d/m/Y - H:i:s', $changed['value']);

    $site_name = Drupal::config('system.site')->get('name');
    $message['subject'] = $site_name . ' - contenu modifié : ' . (($params['entity_type'] == 'node') ? $entity->getTitle() : $entity->getName());
    $message['body'][] = 'Le contenu ' . $url->toString() . ' a été modifié par ' . $user->getDisplayName() . ' le ' . $node_changed;
  }
}

/**
 * Add Columns and change increment primary key to bordeaux_agenda_musee_posted
 */
function bordeaux_agenda_musee_update_9001() {
  $table="bordeaux_agenda_musee_posted";
  $intermediaire_table='bordeaux_agenda_musee_intermediaire_posted';
  $spec = array(
    'description' => 'Primary Key.',
    'fields' => array(
      'id' => array(
        'description' => 'Primary Key, id of the area',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'type' => 'serial',
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'description' => 'Entity Type.',
        'length' => 250,
        'not null' => TRUE,
      ),
      'bundle_id' => array(
        'type' => 'varchar',
        'description' => 'Bundle Id.',
        'length' => 250,
        'not null' => TRUE,
      ),
      'entity_id' => array(
        'type' => 'int',
        'description' => 'Id to Entity .',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'post_id' => array(
        'default' => 0,
        'description' => ' id returned of the posted entity.',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'type' => 'int',
      ),
      'post_date' => array(
        'default' => 0,
        'description' => 'date of the node post',
        'not null' => TRUE,
        'type' => 'int',
      ),
      'mode_test' => array(
        'default' => 0,
        'description' => 'mode test',
        'not null' => TRUE,
        'type' => 'int',
      ),
    ),
    'primary key' => array('id'),
  );

  $schema = Drupal::database()->schema();
  $schema->createTable($intermediaire_table, $spec);

  $query = Drupal::database()->select($table, 'mbp');
  $query->fields('mbp', ['nid', 'post_id', 'post_date', 'mode_test']);
  $results = $query->execute()->fetchAllAssoc('nid');

  foreach ($results as $result) {
    $query = Drupal::database()->insert($intermediaire_table);
    $query->fields(['entity_id', 'entity_type', 'bundle_id', 'post_id', 'post_date', 'mode_test']);
    $entity = Drupal::entityTypeManager()->getStorage('node')->load($result->nid);
    $query->values([$result->nid, 'node', $entity->getType(), $result->post_id, $result->post_date, $result->mode_test]);
    $query->execute();
  }

  $schema = Drupal::database()->schema();
  $schema->dropTable($table);
  $schema->renameTable($intermediaire_table, $table);
}


