<?php

namespace Drupal\bordeaux_agenda_musee\Storage;

use Drupal;

class BordeauxAgendaStorage {
  public static function getAll() {
    $query = Drupal::database()->select('bordeaux_agenda_musee_posted', 'mbp');
    $query->fields('mbp', ['id', 'entity_type','bundle_id','entity_id', 'post_id', 'post_date']);
    $result = $query->execute()->fetchAllAssoc('id');
    return $result;
  }

  public static function exists($nid, $entity_type) {
    $query = Drupal::database()->select('bordeaux_agenda_musee_posted', 'mbp');
    $query->addField('mbp', 'entity_id');
    $query->condition('mbp.entity_id', $nid);
    $query->condition('mbp.entity_type', $entity_type);
    $query->range(0, 1);
    $result = $query->execute()->fetchField();
    return (bool) $result;
  }

  public static function getPostId($nid, $entity_type) {
    $query = Drupal::database()->select('bordeaux_agenda_musee_posted', 'mbp');
    $query->addField('mbp', 'post_id');
    $query->condition('mbp.entity_id', $nid);
    $query->condition('mbp.entity_type', $entity_type);
    $query->range(0, 1);
    $postid = $query->execute()->fetchField();
    return $postid;
  }

  public static function insert($entity_type, $bundle_id, $entity_id, $post_id, $post_ts, $test = 0) {
    $query = Drupal::database()->insert('bordeaux_agenda_musee_posted');
    $query->fields(['entity_type','bundle_id','entity_id','post_id', 'post_date', 'mode_test']);
    $query->values([$entity_type, $bundle_id, $entity_id, $post_id, $post_ts, $test]);
    $query->execute();
  }

  public static function delete($id) {
    $query = Drupal::database()->delete('bordeaux_agenda_musee_posted');
    $query->condition('id', $id);
    $query->execute();
  }

  public static function delete_tests() {
    $query = Drupal::database()->delete('bordeaux_agenda_musee_posted');
    $query->condition('mode_test', 1);
    $query->execute();
  }
}
